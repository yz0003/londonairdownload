'''
Created on 24 May 2018

@author: yz0003
'''

from datalist import *
from sitedata import getelement, getspecies
from wx import EVT_COMBOBOX
from wx.adv import CalendarCtrl, EVT_CALENDAR_SEL_CHANGED
# from wx.lib.calendar import *
import wx
import datetime
import urllib2

class HelloFrame(wx.Frame):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
    
        self.apitext = wx.StaticText(self, label="Choose an API", pos=(20, 20))
        self.apilist = wx.ComboBox(self, value=apis[0], pos=(110, 16), size=(-1, -1), choices=apis, style=wx.CB_DROPDOWN|wx.TE_READONLY)
        self.Bind(EVT_COMBOBOX, self.OnListSelected, self.apilist)
                
        self.sitetext = wx.StaticText(self, label="Sites", pos=(20, 60))
        self.sitelist = wx.ComboBox(self, value="Newham - Cam Road",pos=(110, 56), size=(-1, -1), choices=sitename, style=wx.CB_SORT|wx.TE_READONLY)
        self.Bind(EVT_COMBOBOX, self.OnListSelected, self.sitelist)
        
        self.speciestext = wx.StaticText(self, label="Species", pos=(520, 60))
        self.specieslist = wx.ComboBox(self, value="Nitrogen Dioxide (ug/m3)", pos=(610, 56), size=(-1, -1), choices=speciesname, style=wx.CB_SORT|wx.TE_READONLY)
        self.Bind(EVT_COMBOBOX, self.OnListSelected, self.specieslist)
             
        self.starttext = wx.StaticText(self, label="Start Date", pos=(20, 100))
#         self.labelstart = wx.TextCtrl(self, pos=(110, 96), value="26-07-2012")
        self.labelstart = wx.TextCtrl(self, pos=(110, 96), value="2012-07-26", style=wx.TE_READONLY)
        self.cals = CalendarCtrl(self, pos=(110, 120), date=datetime.date(2012,7, 26))
        self.Bind(EVT_CALENDAR_SEL_CHANGED, self.OnCalsSelected, self.cals)

        self.endtext = wx.StaticText(self, label="End Date", pos=(520, 100))
#         self.labelend = wx.TextCtrl(self, pos=(610, 96), value="03-11-2012")
        self.labelend = wx.TextCtrl(self, pos=(610, 96), value="2012-11-03", style=wx.TE_READONLY)
        self.cale = CalendarCtrl(self, pos=(610, 120), date=datetime.date(2012,11, 3)) # size = (231, 149)
        self.Bind(EVT_CALENDAR_SEL_CHANGED, self.OnCaleSelected,self.cale)
     
        self.startrange = wx.StaticText(self, label="Data Available from: ", pos=(360, 140))
        self.labelsr = wx.TextCtrl(self, pos=(480, 136), style=wx.TE_READONLY, size=(120, -1))
        self.endrange = wx.StaticText(self, label="Data Available to: ", pos=(360, 180))
        self.labeler = wx.TextCtrl(self, pos=(480, 176), style=wx.TE_READONLY, size=(120, -1))
     
        # A button
        self.buttong =wx.Button(self, label="Get", pos=(20, 284))
        self.Bind(wx.EVT_BUTTON, self.OnClickGet,self.buttong)
        
        self.labelapi = wx.TextCtrl(self, pos=(110, 286), size=self.apilist.Size, style=wx.TE_READONLY)
        
        # A button
        self.buttons =wx.Button(self, label="Save", pos=(20, 324))
        self.Bind(wx.EVT_BUTTON, self.OnClickSave,self.buttons)
        self.labelsave = wx.TextCtrl(self, pos=(110, 326), value="data.csv", size=self.apilist.Size)
        
        self.logger = wx.TextCtrl(self, pos=(20,360), size=(920,40), style=wx.TE_MULTILINE | wx.TE_READONLY)
        
        self.data = wx.TextCtrl(self, pos=(20,420), size=(920,260), style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.Updateurl();
        
    def OnListSelected(self, evt):
        self.Updaterange()
        self.Updateurl()
        
    def Updaterange(self):
        apistr = str(self.apilist.GetValue());
        
        ele = getelement(self.sitelist.GetValue())
        if ele:
            if "SpeciesCode" not in apistr:
                self.labelsr.SetValue(ele.get("@DateOpened"))
                self.labeler.SetValue(ele.get("@DateClosed"))
            else:
                species = ele.get("Species")
                speciescode = speciespair.get(self.specieslist.GetValue())
                spe = getspecies(species, speciescode)
                if spe:
                    self.labelsr.SetValue(spe.get("@DateMeasurementStarted"))
                    self.labeler.SetValue(spe.get("@DateMeasurementFinished"))
                else:
                    self.logger.AppendText("No such Species: %s\n" %self.specieslist.GetValue()) 
#                     self.logger.SetForegroundColour(wx.RED)
                    self.labelsr.SetValue("")
                    self.labeler.SetValue("")
        else:
            self.logger.AppendText("No such Site: %s\n" %self.sitelist.GetValue())
            self.labelsr.SetValue("")
            self.labeler.SetValue("")
            
         
    def Updateurl(self):
        apistr = str(self.apilist.GetValue());
        
        sitecode = sitepair.get(self.sitelist.GetValue())
        startdate = self.labelstart.GetValue()
        enddate = self.labelend.GetValue()
        
        apistr = apistr.replace(sc, sitecode)
        apistr = apistr.replace(sd, startdate)
        apistr = apistr.replace(ed, enddate)
        
        if "SpeciesCode" not in apistr:
            self.specieslist.Disable()
            savestr = sitecode +"_"+ startdate +"_"+ enddate + self.tail(apistr)
        else:
            self.specieslist.Enable(enable=True)
            speciescode = speciespair.get(self.specieslist.GetValue())
            apistr = apistr.replace(spc, speciescode)
            savestr = sitecode +"_"+ speciescode +"_"+ startdate +"_"+ enddate + self.tail(apistr)
            
        self.labelsave.SetValue(savestr)
        self.labelapi.SetValue(apistr)
        
    def tail(self, apistr):
        if "Json" in apistr:
            return ".json"
        if "csv" in apistr:
            return ".csv"
        return ".xml"
     
    def FormatDate(self, date):
#         return date.Format(format="%d-%m-%Y")
        return date.Format(format="%Y-%m-%d")
    
    def OnClickGet(self,event):
        url = self.labelapi.GetValue()
        self.logger.AppendText("Send request to API: %s\n" %url)
        contents = urllib2.urlopen(url).read()
        self.data.SetValue(contents)
    
    def OnClickSave(self,event):
        filename = self.labelsave.GetValue()
        try:
            f = open(filename,"w") 
            if "csv" in filename:
                text = self.data.GetValue()
                index = text.rfind("\n")
                text = text[0:index]
                index = text.rfind("\n")
                f.write(text[0:index]+"\n")
            else:
                f.write(self.data.GetValue())
            self.logger.AppendText("Save Data in %s\n" %filename)
        except IOError:
            self.logger.AppendText("Error: %s\n" %str(IOError))
        finally:
            f.close()
            return
        
    def OnCalsSelected(self, evt):
        sdate = evt.GetDate()
        text = self.FormatDate(sdate)
        self.labelstart.SetLabel(text)
        self.Updateurl();    
        
    def OnCaleSelected(self, evt):
        edate = evt.GetDate()
        text = self.FormatDate(edate)
        self.labelend.SetLabel(text) 
        self.Updateurl();   
        

if __name__ == '__main__':
    app = wx.App()
    frm = HelloFrame(None)
    frm.SetTitle("Get data from London Air API")
    frm.SetSize(200, 200, 980, 720)
#     frm.ToggleWindowStyle(wx.STAY_ON_TOP)
    frm.Show()
    app.MainLoop()