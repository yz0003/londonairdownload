'''
Created on 25 May 2018

@author: yz0003
'''

import json

def getelement(sitename):
    with open('sitespecies.json') as json_data:
        data = json.load(json_data)
        for element in data:
            if element["@SiteName"] == sitename:
                return element

def getspecies(species, speciescode):
    for element in species:
        if element["@SpeciesCode"] == speciescode:
            return element
    